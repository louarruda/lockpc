program lockPC;

uses
  Forms,
  Unit2 in 'Unit2.pas' {frmMain},
  USock in 'USock.pas',
  uLibINI in 'uLibINI.pas',
  uDM in 'uDM.pas' {dm: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'PC - Lock';
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
