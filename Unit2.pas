unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls,
  Buttons, Gauges, ComCtrls, Menus, CoolTrayIcon, Spin, dxGDIPlusClasses,
  ExtDlgs, jpeg, ClipBrd;

type

  THour = Record
    iMin : Integer;
    iHour   : Integer;
  end;

  TDay = Record
    b1  : Boolean;
    b2  : Boolean;
    b3  : Boolean;
    b4  : Boolean;
    b5  : Boolean;
    b6  : Boolean;
    b7  : Boolean;
  end;

  TImagePath = Record
    sImgDefaul : String;
    sImgLocked : String;
  end;

  TLockPC = Record
    iSecWait    : Integer;
    oStartTime  : THour;
    oEndTime    : THour;
    oDays       : TDay;
    oImages     : TImagePath;
  end;

  TfrmMain = class(TForm)
    pgcMain: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    edtStartHour: TSpinEdit;
    edtStartMin: TSpinEdit;
    edtEndHour: TSpinEdit;
    edtEndMin: TSpinEdit;
    Shape1: TShape;
    ckb2: TCheckBox;
    ckb3: TCheckBox;
    ckb4: TCheckBox;
    ckb5: TCheckBox;
    ckb6: TCheckBox;
    ckb7: TCheckBox;
    ckb1: TCheckBox;
    pgb: TGauge;
    TabSheet2: TTabSheet;
    imgDefaul: TImage;
    imgLocked: TImage;
    TabSheet3: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edtSecWait: TSpinEdit;
    CoolTrayIcon1: TCoolTrayIcon;
    SpeedButton1: TSpeedButton;
    edtImgDefaul: TEdit;
    edtImgLocked: TEdit;
    Panel1: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    TabSheet4: TTabSheet;
    memoClipboard: TMemo;
    CheckBox1: TCheckBox;
    btnFace1: TSpeedButton;
    btnFace2: TSpeedButton;
    btnClipboardRestore: TSpeedButton;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure CoolTrayIcon1DblClick(Sender: TObject);
    procedure imgDefaulClick(Sender: TObject);
    procedure imgLockedClick(Sender: TObject);
    procedure btnFace1Click(Sender: TObject);
    procedure btnClipboardRestoreClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    procedure preparaTela;
    procedure lerValores;
    procedure salvarConfiguracoes;
  public
    //
  end;

var
  frmMain: TfrmMain;
  oConf : TLockPC;

implementation

uses ScktComp, USock, uLibINI, uDM;

{$R *.dfm}

procedure TfrmMain.salvarConfiguracoes;
var sIni : String;
begin
  sIni := ExtractFilePath(Application.ExeName) + 'lockPC.ini';
  storeIniValue(sIni, 'time', 'max', edtSecWait.Value);

  storeIniValue(sIni, 'start', 'h', edtStartHour.Value);
  storeIniValue(sIni, 'start', 'm', edtStartMin.Value);

  storeIniValue(sIni, 'end', 'h',  edtEndHour.Value);
  storeIniValue(sIni, 'end', 'm',  edtEndMin.Value);

  storeIniValue(sIni, 'w_day', '1', ckb1.Checked);
  storeIniValue(sIni, 'w_day', '2', ckb2.Checked);
  storeIniValue(sIni, 'w_day', '3', ckb3.Checked);
  storeIniValue(sIni, 'w_day', '4', ckb4.Checked);
  storeIniValue(sIni, 'w_day', '5', ckb5.Checked);
  storeIniValue(sIni, 'w_day', '6', ckb6.Checked);
  storeIniValue(sIni, 'w_day', '7', ckb7.Checked);

  storeIniValue(sIni, 'wallpaper', 'default', edtImgDefaul.Text);
  storeIniValue(sIni, 'wallpaper', 'locked',    edtImgLocked.Text);
end;

procedure TfrmMain.SpeedButton1Click(Sender: TObject);
begin
  salvarConfiguracoes;
  lerValores;
  preparaTela;
end;

procedure TfrmMain.btnClipboardRestoreClick(Sender: TObject);
begin
  dm.StrToClipbrd(memoClipboard.Lines.Text);
end;

procedure TfrmMain.btnFace1Click(Sender: TObject);
begin
  memoClipboard.Lines.Text := Clipboard.AsText;
  dm.StrToClipbrd(TSpeedButton(Sender).Caption);
end;

procedure TfrmMain.CoolTrayIcon1DblClick(Sender: TObject);
begin
  CoolTrayIcon1.ShowMainForm;
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := False;
  dm.Minimizar1Click(dm.Minimizar1);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  lerValores;
  preparaTela;

  if (ParamStr(1) = 'locknow') or (ParamStr(1) = 'lock') or (ParamStr(1) = 'ln') then
  begin
    dm.BloquearPCAgora1Click(dm.BloquearPCAgora1);
  end
  else if (ParamStr(1) = 'wp') or (ParamStr(1) = 'wallpaper') then
  begin
    Application.Terminate; // open just for validade wallpaper
  end;
end;

procedure TfrmMain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    49 : if ssCtrl in  Shift then
            pgcMain.ActivePageIndex := 3;

  end;
end;

procedure TfrmMain.imgLockedClick(Sender: TObject);
begin
  if dm.OpenPictureDialog1.Execute = True then
  begin
    edtImgLocked.Text := dm.OpenPictureDialog1.FileName;
    imgLocked.Picture.LoadFromFile(edtImgLocked.Text);
  end;
end;

procedure TfrmMain.imgDefaulClick(Sender: TObject);
begin
  if dm.OpenPictureDialog1.Execute = True then
  begin
    edtImgDefaul.Text := dm.OpenPictureDialog1.FileName;
    imgDefaul.Picture.LoadFromFile(edtImgDefaul.Text);
  end;
end;

procedure TfrmMain.lerValores;
var sIni : String;
begin
  sIni := ExtractFilePath(Application.ExeName) + 'lockPC.ini';

  oConf.iSecWait := restoreIniValue(sIni, 'time', 'max',  130);

  oConf.oStartTime.iHour  :=  restoreIniValue(sIni, 'start', 'h',   07);
  oConf.oStartTime.iMin   :=  restoreIniValue(sIni, 'start', 'm',   00);

  oConf.oEndTime.iHour  :=  restoreIniValue(sIni, 'end', 'h',     19);
  oConf.oEndTime.iMin   :=  restoreIniValue(sIni, 'end', 'm',     00);

  oConf.oDays.b1 :=  restoreIniValue(sIni, 'w_day', '1',   True);
  oConf.oDays.b2 :=  restoreIniValue(sIni, 'w_day', '2',   True);
  oConf.oDays.b3 :=  restoreIniValue(sIni, 'w_day', '3',   True);
  oConf.oDays.b4 :=  restoreIniValue(sIni, 'w_day', '4',   True);
  oConf.oDays.b5 :=  restoreIniValue(sIni, 'w_day', '5',   True);
  oConf.oDays.b6 :=  restoreIniValue(sIni, 'w_day', '6',   True);
  oConf.oDays.b7 :=  restoreIniValue(sIni, 'w_day', '7',   True);

  oConf.oImages.sImgDefaul := restoreIniValue(sIni, 'wallpaper', 'default', '');
  oConf.oImages.sImgLocked := restoreIniValue(sIni, 'wallpaper', 'locked',  '');
end;

procedure TfrmMain.preparaTela;
begin
  frmMain.Width  := 364;
  frmMain.Height := 352;

  pgcMain.ActivePageIndex := 0;

  pgb.Progress  := oConf.iSecWait;
  pgb.MinValue  := 0;
  pgb.MaxValue  := oConf.iSecWait;

  edtSecWait.Value := oConf.iSecWait;

  edtStartHour.Value   := oConf.oStartTime.iHour;
  edtStartMin.Value := oConf.oStartTime.iMin;

  edtEndHour.Value   := oConf.oEndTime.iHour;
  edtEndMin.Value := oConf.oEndTime.iMin;

  ckb1.Checked  := oConf.oDays.b1;
  ckb2.Checked  := oConf.oDays.b2;
  ckb3.Checked  := oConf.oDays.b3;
  ckb4.Checked  := oConf.oDays.b4;
  ckb5.Checked  := oConf.oDays.b5;
  ckb6.Checked  := oConf.oDays.b6;
  ckb7.Checked  := oConf.oDays.b7;

  if (oConf.oImages.sImgDefaul <> '') then
  begin
    edtImgDefaul.Text := oConf.oImages.sImgDefaul;
    imgDefaul.Picture.LoadFromFile(oConf.oImages.sImgDefaul);
  end;

  if (oConf.oImages.sImgLocked <> '') then
  begin
    edtImgLocked.Text := oConf.oImages.sImgLocked;
    imgLocked.Picture.LoadFromFile(oConf.oImages.sImgLocked);
  end;

  dm.Validarpapeldeparede1Click(dm.Validarpapeldeparede1);

  Application.ProcessMessages;
end;

end.
