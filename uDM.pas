unit uDM;

interface

uses
  SysUtils, Classes, Menus, CoolTrayIcon, ExtCtrls, Forms, Windows, Dialogs,
  ExtDlgs, Registry, ClipBrd;

type
  Tdm = class(TDataModule)
    Timer1: TTimer;
    PopupMenu1: TPopupMenu;
    Minimizar1: TMenuItem;
    N1: TMenuItem;
    Encerra1: TMenuItem;
    tmrInicial: TTimer;
    OpenPictureDialog1: TOpenPictureDialog;
    N2: TMenuItem;
    BloquearPCAgora1: TMenuItem;
    Validarpapeldeparede1: TMenuItem;
    procedure Timer1Timer(Sender: TObject);
    procedure Minimizar1Click(Sender: TObject);
    procedure Encerra1Click(Sender: TObject);
    procedure tmrInicialTimer(Sender: TObject);
    procedure BloquearPCAgora1Click(Sender: TObject);
    procedure Validarpapeldeparede1Click(Sender: TObject);
  private
    function bloqueiaHoje: Boolean;
    function bloqueiaHora: boolean;
    procedure lock;
    function SecondsIdle: DWord;
    procedure wallpaperChange(pImagemBMP: string; pTile: Boolean);
    procedure wallpaperValidade;
  public
    procedure StrToClipbrd(StrValue: string);
  end;

var
  dm: Tdm;

implementation

uses Unit2;

{$R *.dfm}

procedure Tdm.Encerra1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure Tdm.Minimizar1Click(Sender: TObject);
begin
  if frmMain.Visible = False then
    frmMain.CoolTrayIcon1.ShowMainForm
  else
  begin
    frmMain.CoolTrayIcon1.HideMainForm;
    Application.Minimize;
  end;
end;

procedure Tdm.Timer1Timer(Sender: TObject);
var bBloqueiaData : Boolean;
begin
  bBloqueiaData := bloqueiaHoje;

  if (bBloqueiaData and bloqueiaHora) then
  begin
    if (SecondsIdle > oConf.iSecWait) then
      lock;

    frmMain.pgb.Progress := oConf.iSecWait - SecondsIdle;
  end;
end;

procedure Tdm.tmrInicialTimer(Sender: TObject);
begin
  tmrInicial.Enabled := False;
  Minimizar1Click(Minimizar1);
end;

procedure Tdm.Validarpapeldeparede1Click(Sender: TObject);
begin
  wallpaperValidade;
end;

procedure Tdm.BloquearPCAgora1Click(Sender: TObject);
begin
  lock;
  Application.Terminate;
end;

function Tdm.bloqueiaHoje : Boolean;
begin
  with frmMain do
    case DayOfWeek(Date) of
      1 : Result := ckb1.Checked;
      2 : Result := ckb2.Checked;
      3 : Result := ckb3.Checked;
      4 : Result := ckb4.Checked;
      5 : Result := ckb5.Checked;
      6 : Result := ckb6.Checked;
      7 : Result := ckb7.Checked;
      else Result := False;
    end;
end;

function Tdm.bloqueiaHora : boolean;
var tAgora : TTime;
begin
  with frmMain do
  begin
    tAgora := Time;
    if (tAgora >=  StrToTime(IntToStr(edtStartHour.Value) + ':' + IntToStr(edtStartMin.Value))) and
       (tAgora <= StrToTime(IntToStr(edtEndHour.Value)    + ':' + IntToStr(edtEndMin.Value)))
    then
      Result := True
    else
      Result := False;
  end;
end;

procedure Tdm.lock;
begin
  Windows.LockWorkStation;
end;

function Tdm.SecondsIdle: DWord;
var
   liInfo: TLastInputInfo;
begin
  liInfo.cbSize := SizeOf(TLastInputInfo) ;
  GetLastInputInfo(liInfo) ;
  Result := (GetTickCount - liInfo.dwTime) DIV 1000;
end;

procedure Tdm.wallpaperChange(pImagemBMP: string; pTile: Boolean);
var Reg : TRegIniFile;
begin
	reg := TRegIniFile.Create('Control Panel\Desktop');
	with Reg do
	begin
		WriteString('', 'Wallpaper', pImagemBMP);
		if (pTile) then
			WriteString('', 'TileWallpaper', '1')
		else
			WriteString('', 'TileWallpaper', '0')
	end;

	Reg.Free;
	SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, nil, SPIF_SENDWININICHANGE);
end;

procedure Tdm.wallpaperValidade;
var bBloqueiaData : Boolean;
begin
  bBloqueiaData := bloqueiaHoje;

  if (bBloqueiaData and bloqueiaHora) then
  begin
    if oConf.oImages.sImgLocked <> '' then
      wallpaperChange(oConf.oImages.sImgLocked, false);
  end
  else
  begin
    if oConf.oImages.sImgDefaul <> '' then
      wallpaperChange(oConf.oImages.sImgDefaul, false);
  end;
end;

procedure Tdm.StrToClipbrd(StrValue: string);
var
  S: string;
  hMem: THandle;
  pMem: PChar;
begin
  //
end;

end.
