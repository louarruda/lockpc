Aplicativo auxiliar quem utiliza o mesmo computador para o trabalho e para o uso pessoal.

-> Com ele é possível definir um horário um período em que o computador faça o "lock" de forma automática após "x" segundos.
-> Fixar papeis de parede diferentes para dentro e fora do período especificado.
-> Uma alternativa ao bug "Can not create file" do delphi, após algumas atualizações do windows (Ainda não implementado e será baseado no código do "dzEditorLineEndsFix".
-> Alguns extras (Ainda não implementados)
    -> Multi copy&paste
    -> Copiar alguns ascii emoticons para o clipboard


Para desenvolvimento foi utilizado o Delphi 2010 com o componente coolTrayIcon (openSource)